//
// Created by vincent on 1/4/22.
//

#include "uECC.h"
#include "sha3.h"


void print_hex2ascii(uint8_t * data, int len)
{
  for(int i=0; i<len; i++){
    printf("%02x", data[i]);
  }
}

static int default_RNG(uint8_t *dest, unsigned int size) {

  uint32_t tick = HAL_GetTick();
  uint8_t * ptick = ((uint8_t*)tick);
  uint8_t i=0, j=0;

  while(i<size){
    dest[i++] = ptick[j++];
    if((i%4)==0){
      j = 0;
    }
  }

  return 1;
}

char * ascii2hex(char *out, const char *in, size_t len) {
  unsigned int i, t, hn, ln;
  for (t = 0,i = 0; i < len; i+=2,++t) {
    hn = in[i]   > '9' ? (in[i]  |32) - 'a' + 10 : in[i]   - '0';
    ln = in[i+1] > '9' ? (in[i+1]|32) - 'a' + 10 : in[i+1] - '0';
    out[t] = (hn << 4) | ln;
  }
  return out;
}



#define ETHEREUM_PRIVATE_KEY          "6379203b0407e58a6b585160ce27c88d9b512c4f8c8b9a5592ccc8ea95770ab0"
#define ETHEREUM_ADDRESS_RECIPIENT    "4ba6ec46f0cebfd805cc521d84bb6df4af182df8"
#define ETHEREUM_TRANSACTION_RLP_NOSIGNATURE_SIZE     52 //bytes
#define ETHEREUM_TRANSACTION_RLP_WITHSIGNATURE_SIZE   120 //bytes


int testMicroECC()
{
  uint8_t private[32] = {0};
  //uint8_t public[64] = {0};
  uint8_t sig[64] = {0};
  uint8_t gas[16] = {0};
  uint8_t RLP[ETHEREUM_TRANSACTION_RLP_WITHSIGNATURE_SIZE] = {0};

  /// header
  RLP[1] = 0xF3;

  /// nonce
  RLP[2] = 0x3A;

  /// gasPrice
  RLP[3] = 0x86;
  ascii2hex((char*)&gas, "09184e72a000", 12);
  memcpy(&RLP[04], gas, 6);

  /// gasLimit
  RLP[10] = 0x83;
  ascii2hex((char*)&gas, "030000", 6);
  memcpy(&RLP[11], gas, 3);

  /// recipient address (40 bytes)
  RLP[14] = 0x94;
  ascii2hex((char*)&RLP[15], ETHEREUM_ADDRESS_RECIPIENT, 40);

  /// value (0 ETH)
  RLP[35] = 0x80;

  /// data (16 bytes reserved for payload)
  RLP[36] = 0x90;

  /// chainId or "V"
  RLP[53] = 0x1C; //TODO : recovery identifier 1B or 1C (R or R' ephemeral public key Q)

  
  /// Building TX process :
  
  printf("\n\nBuilding Ethereum Transaction on STM32L0 :");

  printf("\n> RLP non-signed : 0x");
  print_hex2ascii(RLP+1, ETHEREUM_TRANSACTION_RLP_NOSIGNATURE_SIZE);

  sha3_context c;
  sha3_Init256(&c);
  sha3_SetFlags(&c, SHA3_FLAGS_KECCAK);
  sha3_Update(&c, RLP+1, ETHEREUM_TRANSACTION_RLP_NOSIGNATURE_SIZE);
  const uint8_t * hashKeccak256 = sha3_Finalize(&c);

  printf("\n> hashKeccak256  : 0x");
  print_hex2ascii(hashKeccak256, 32);



  uECC_set_rng(default_RNG); // set the Random Number Generator for uECC_sign()

  ascii2hex(private, ETHEREUM_PRIVATE_KEY, 64);
  printf("\n> private key    : 0x");
  print_hex2ascii(private, 32);

/*
  if(uECC_compute_public_key(private, public, uECC_secp256k1())){
    printf("\n> public  key    : 0x");
    print_hex2ascii(public, 64);
  } else {
    printf("\n ERROR : uECC_compute_public_key() failed !\n");
  }*/


  if(uECC_sign(private, hashKeccak256, 32, sig, uECC_secp256k1())) {
    printf("\n> sig R          : 0x");
    print_hex2ascii(sig, 32);
    printf("\n> sig S          : 0x");
    print_hex2ascii(sig+32, 32);
  } else {
    printf("\n ERROR : uECC_sign() failed ! \n");
  }


  /// header (rewrite)
  RLP[0] = 0xF8;
  RLP[1] = 0x76;

  /// ECDSA "R"
  RLP[54] = 0xA0;
  memcpy(&RLP[55], sig, 32);

  /// ECDSA "S"
  RLP[87] = 0xA0;
  memcpy(&RLP[88], sig+32, 32);

  printf("\n> RLP signed     : 0x");
  print_hex2ascii(RLP, ETHEREUM_TRANSACTION_RLP_WITHSIGNATURE_SIZE);

  printf("\n\n");
  return 0;
}




---
Author: Vincent Ollivier, ollivierv@gmail.com   
Date:	Jan, 2022
---  

# STM32L0 Elliptic-curve cryptography

STM32L0 Elliptic-curve cryptography example

# License

 - uECC : Copyright 2014, Kenneth MacKay
 - SHA3 : Andrey Jivsov. crypto@brainhub.org